<?php
/**
 * Modular Simply Framework
 *
 * An open source application development framework for PHP 5.3.9 or newer
 *
 * @author		Jarosław Furmanek
 * @copyright	Copyright (c) 2015, Jarosław Furmanek
 * @license		MIT License
 * @since		Version 0.9.0a
 */
 
namespace MSF
{
	class Theme
	{
		private $file_body = 'body';
		private $values = array();
		
		public function __construct($name)
		{
			define("THEME_PATH", "site/themes/".$name);
			define("THEME_URL", (app()->config('path') != '' ? "/".app()->config('path') :'')."/site/themes/".$name);
			
			$this->values = array(
				'title' => app()->config('page_name'),
				'page_title' => '',
				'description' => app()->config('page_description'),
				'style' => '',
				'script' => '',
			);
		}
		
		public function set_title($name)
		{
			$this->values['title'] = $name.' | '.app()->config('page_name');
			$this->values['page_title'] = $name;
		}
		
		public function set_description($description)
		{
			$this->values['description'] = $desc;
		}
		
		public function set_file_body($file)
		{
			$this->file_body = $file;
		}
		
		public function set_value($name, $value) 
		{
			$this->values[$name] = $value;
		}
		
		public function add_style($file, $version = '1.0')
		{
			$this->values['style'] .= "\n<link rel='stylesheet' href='$file?v=$version'>";
		}
		
		public function add_script($file, $source = false, $version = '1.0')
		{
			if ($source)
				$this->values['script'] .= "\n".$file;
			else
				$this->values['script'] .= "\n<script src='$file?v=$version' ></script>";
		}
		
		private function load_view($_name, $_args = null)
		{
			if ($_args)
				extract($_args,EXTR_SKIP,'theme');
			include THEME_PATH.'/'.$_name.'.php';
		}
		
		public function generate_page($content)
		{
			if ($this->file_body !== FALSE)
			{
				require THEME_PATH.'/theme.php';
				
				ob_start();
				
				$this->values['content'] = $content;
				
				$this->load_view($this->file_body, $this->values);
				
				$this->values['body'] = ob_get_contents();
			    ob_end_clean();

				$this->load_view('html', $this->values);
			}
			else {
				echo $content;
			}
		}
	}
}
?>