<?php 
/**
 * Modular Simply Framework
 *
 * An open source application development framework for PHP 5.3.9 or newer
 *
 * @author		Jarosław Furmanek
 * @copyright	Copyright (c) 2015, Jarosław Furmanek
 * @license		MIT License
 * @since		Version 0.9.0a
 */
 
namespace MSF
{
	abstract class _Variables
	{
		protected function is_set($vars, $type)
		{
			if (is_array($vars))
			{
				foreach ($vars as $var) 
					if (!filter_has_var($type, $var))
						return false;
				
				return true;
			}
			else 
				return filter_has_var($type, $vars);
		}
		
		protected function is_int($vars, $arr)
		{
			if (is_array($vars))
			{
				foreach ($vars as $var) 
					if (!(preg_match("/^[-]?[0-9]+$/", $arr[$var])))
						return false;
				
				return true;
			}
			else 
				return (preg_match("/^[-]?[0-9]+$/", $arr[$vars]));
		}

		protected function is_double($vars, $arr)
		{
			if (is_array($vars))
			{
				foreach ($vars as $var) 
					if (!(preg_match("/^[-]?[0-9]+(.[0-9]+)*$/", $arr[$var])))
						return false;
				
				return true;
			}
			else 
				return (preg_match("/^[-]?[0-9]+(.[0-9]+)*$/", $arr[$vars]));
		}
		
		protected function filter_text($vars, $arr)
		{
			$search = array("&", "\"", "'", "\\", '\"', "\'", "<", ">", "`");
			$replace = array("&amp;", "&quot;", "&#39;", "&#92;", "&quot;", "&#39;", "&lt;", "&gt;", "&lsquo");
			
			if (is_array($vars))
			{
				$result = null;
				foreach ($vars as $value) 
					$result[$value] = str_replace($search, $replace, trim($arr[$value]));
				
				return $result;
			}
			else 
				return str_replace($search, $replace, trim($vars));
		}
	}
		
	class Post extends _Variables
	{	
		public function is_set($vars)
		{
			return _Variables::is_set($vars, INPUT_POST);
		}
		
		public function is_int($vars)
		{
			if ($this->is_set($vars))
				return _Variables::is_int($vars, $_POST);
			return false;
		}
		
		public function is_double($vars)
		{
			if ($this->is_set($vars))
				return _Variables::is_double($vars, $_POST);
			return false;
		}
		
		public function filter_text($vars)
		{
			if ($this->is_set($vars))
				return _Variables::filter_text($vars, $_POST);
			return NULL;
		}
		
		public function check($name, $value)
		{
			return (($this->is_set($name)) && ($_POST[$name] == $value));
		}
	}
	
	class Get extends _Variables
	{	
		public function is_set($vars)
		{
			return _Variables::is_set($vars, INPUT_GET);
		}
		
		public function is_int($vars)
		{
			if ($this->is_set($vars))
				return _Variables::is_int($vars, $_GET);
			return false;
		}
		
		public function is_double($vars)
		{
			if ($this->is_set($vars))
				return _Variables::is_double($vars, $_GET);
			return false;
		}
		
		public function filter_text($vars)
		{
			if ($this->is_set($vars))
				return _Variables::filter_text($vars, $_GET);
			return NULL;
		}
		
		public function check($name, $value)
		{
			return (($this->is_set($name)) && ($_GET[$name] == $value));
		}
	}
	
	class Cookie extends _Variables
	{	
		public function is_set($vars)
		{
			return _Variables::is_set($vars, INPUT_COOKIE);
		}
		
		public function is_int($vars)
		{
			if ($this->is_set($vars))
				return _Variables::is_int($vars, $_COOKIE);
			return false;
		}
			
		public function is_double($vars)
		{
			if ($this->is_set($vars))
				return _Variables::is_double($vars, $_COOKIE);
			return false;
		}
		
		public function filter_text($vars)
		{
			if ($this->is_set($vars))
				return _Variables::filter_text($vars, $_COOKIE);
			return NULL;
		}
		
		public function check($name, $value)
		{
			return (($this->is_set($name)) && ($_COOKIE[$name] == $value));
		}
	}
	
	class Variable extends _Variables
	{
		public function is_int($vars)
		{
			if (is_array($vars))
			{
				foreach ($vars as $var) 
					if (!(preg_match("/^[-]?[0-9]+$/", $var)))
						return false;
				
				return true;
			}
			else 
				return (preg_match("/^[-]?[0-9]+$/", $vars));
		}
		
		public function is_double($vars)
		{
			if (is_array($vars))
			{
				foreach ($vars as $var) 
					if (!(preg_match("/^[-]?[0-9]+(.[0-9]+)*$/", $var)))
						return false;
				
				return true;
			}
			else 
				return (preg_match("/^[-]?[0-9]+(.[0-9]+)*$/", $vars));
		}
		
		public function filter_text($vars)
		{
			$search = array("&", "\"", "'", "\\", '\"', "\'", "<", ">", "`");
			$replace = array("&amp;", "&quot;", "&#39;", "&#92;", "&quot;", "&#39;", "&lt;", "&gt;", "&lsquo");
			
			if (is_array($vars))
			{
				$result = null;
				foreach ($vars as $key => $value) 
					$result[$key] = str_replace($search, $replace, trim($value));
				
				return $result;
			}
			else 
				return str_replace($search, $replace, trim($vars));
		}
		
		public function check($name, $value)
		{
			return (($this->is_set($name)) && ($_COOKIE[$name] == $value));
		}
				
		function is_email($email)
		{
			if(filter_var($email, FILTER_VALIDATE_EMAIL))
			{
			 	list($username,$domain)=explode('@',$email);
					
				if(!checkdnsrr($domain,'MX')) 
			  		return false;
			 	 return true;
			 }
		 	 return false;
		}
	}
	
	class Variables
	{
		private $post;
		private $get;
		private $cookie;
		private $variable;
		
		public function __construct()
		{
			$this->post = new Post();
			$this->get = new Post();
			$this->cookie = new Cookie();
			$this->variable = new Variable();
		}
		
		public function post() 
		{
			return $this->post;
		}

		public function get() 
		{
			return $this->get;
		}
		
		public function cookie() 
		{
			return $this->cookie;
		}
		
		public function variable() 
		{
			return $this->variable;
		}
	}
}

?>