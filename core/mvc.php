<?php
/**
 * Modular Simply Framework
 *
 * An open source application development framework for PHP 5.3.9 or newer
 *
 * @author		Jarosław Furmanek
 * @copyright	Copyright (c) 2015, Jarosław Furmanek
 * @license		MIT License
 * @since		Version 0.9.0a
 */
 
 
abstract class _Controller
{
	public $app;
	private $name_module;
	
	public function __construct()
	{
		global $page;
		$this->app = app();
		$this->name_module = $this->app->get_part_addres(0);
	}
	
	public function redirect($address = null)
	{
		if (!$address)
			$address = '/'.$this->name_module.'/'.$this->app->get_part_addres(1);
		$this->app->redirect($address);
	}
	
	protected function load_model($name = null, $module = null)
	{
		$name = $name ?: $this->name_module;
		$module = $module ?: $this->name_module;
		include 'site/modules/'.$module.'/m_'.$name.'.php';
		$name = 'M_'.$name;
		return new $name();
	}
	
	protected function load_view($_name = 'index', $_args = null, $_module = null)
	{
		$_module = $_module ?: $this->name_module;
		
		if ($_args)
			extract($_args,EXTR_SKIP,'mod');
		include 'site/modules/'.$_module.'/v_'.$_name.'.php';
	}
	
	public function _config()
	{
		return null;
	}
	
	public function index()
	{
		$this->app->redirect('/404');
	}
}

abstract class _Model 
{
	protected $db;
	
	public function __construct()
	{
		global $app;
		$this->db = $app->get_db();
	}
	
}

?>