<?php
/**
 * Modular Simply Framework
 *
 * An open source application development framework for PHP 5.3.9 or newer
 *
 * @author		Jarosław Furmanek
 * @copyright	Copyright (c) 2015, Jarosław Furmanek
 * @license		MIT License
 * @since		Version 0.9.0a
 */
 
namespace MSF
{
	class Database {
		
		private $mysqli;
		private $prefix;
		
		public function __construct($id = 0)
		{
			global $database;
			
			if (count($database[$id]) >= 4)
			{
				@ $this->mysqli = new \mysqli($database[$id]['host'], $database[$id]['user'], $database[$id]['pwd'], $database[$id]['dbname']);
				if ($this->mysqli->connect_errno) die ('Unable to connect to database');
				$this->query('SET NAMES "'.$database[$id]['charset'].'"');
				
				$this->prefix = &$database[$id]['prefix'];
			}
			else
			{
				die ('Unable to connect to database');
			}
		}
		
		public function close()
		{
			$this->mysqli->close();
		}
		
		public function get_mysqli()
		{
			return $this->mysqli;
		}
	
		public function get_prefix()
		{
			return $this->prefix;
		}
		
		public function query($sql)
		{
			return $this->mysqli->query($sql);
		}
		
		public function select($from, $select='*', $where = NULL, $order = NULL, $limit = NULL) 
		{
			$query='SELECT '.$select.' FROM '.$this->prefix.$from;
			if($where!=NULL)
				$query.=' WHERE '.$where;
			if($order!=NULL)
				$query.=' ORDER BY '.$order;
			if($limit!=NULL)
				$query.=' LIMIT '.$limit;
			 
			return $this->query($query);
		}
		
		public function num_rows($from, $where = NULL)
		{
			$query='SELECT COUNT(*) FROM '.$this->prefix.$from;
			if($where!=NULL)
				$query.=' WHERE '.$where;
			 
			$return = $this->query($query)->fetch_assoc();
			return $return['COUNT(*)'];
		}
		
		public function insert($into, $values_arr)
		{
			return $this->query('INSERT INTO '.$this->prefix.$into.' ('.implode(',',array_keys($values_arr)).') VALUES ("'.implode('","',array_values($values_arr)).'")');
		}
		
		public function delete($from, $where = NULL, $order = NULL, $limit = NULL)
		{
			$query='DELETE FROM '.$this->prefix.$from;
			if($where!=NULL)
				$query.=' WHERE '.$where;
			if($order!=NULL)
				$query.=' ORDER BY '.$order;
			if($limit!=NULL)
				$query.=' LIMIT '.$limit;
			 
			return $this->query($query);
		}
		
		public function update($update, $values_arr, $where = NULL)
		{
			$query='UPDATE '.$this->prefix.$update. ' SET ';
			
			$first = true;
			
			foreach ($values_arr as $key => $value) 
			{
				if ($first)
				{
					$first = false;
					$query .= $key.'="'.$value.'"';
				}
				else
					$query .= ','.$key.'="'.$value.'"';
			}
			
			if($where!=NULL)
				$query.=' WHERE '.$where;
	
			return $this->query($query);
		}
	}
}
?>