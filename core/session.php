<?php
/**
 * Modular Simply Framework
 *
 * An open source application development framework for PHP 5.3.9 or newer
 *
 * @author		Jarosław Furmanek
 * @copyright	Copyright (c) 2015, Jarosław Furmanek
 * @license		MIT License
 * @since		Version 0.9.0a
 */

namespace MSF
{
	class Session
	{	
		public function __construct()
		{
			
			if (isset($_GET['remember']))
				ini_set("session.cookie_lifetime", 1209600);
			
			session_start();
			
			if (isset($_GET['remember']))
				session_regenerate_id();
			
			if (!isset( $_SESSION['msf'] )) 
			{
				session_regenerate_id();
				$_SESSION['msf'] = true;
			}
			
		}
		
		public function add($name, $value)
		{
			$_SESSION[$name] = $value;
		}
		
		public function delete($name)
		{
			unset($_SESSION[$name]);
		}
		
		public function get($name)
		{
			if (isset($_SESSION[$name]))
				return $_SESSION[$name];
			else
				return null; 
		}
		
		public function destroy()
		{
			session_destroy();
		}
		
		public function regenerate($delete_old_session = false)
		{
			session_regenerate_id($delete_old_session);
		}
	}
}
?>