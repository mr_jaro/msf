<?php
/**
 * Modular Simply Framework
 *
 * An open source application development framework for PHP 5.3.9 or newer
 *
 * @author		Jarosław Furmanek
 * @copyright	Copyright (c) 2015, Jarosław Furmanek
 * @license		MIT License
 * @since		Version 0.9.0a
 */
 
class DatabaseSessionHandler 
{
	private $db;
	private $session_table;
	
	public function __construct($db)
	{
		global $config;
		$this->db = $db;
		$this->session_table = $config['session_table'];
	}
	
	function open ($save_path, $session_name) 
	{
		return true;
	}
	
	function close() 
	{
		return true;
	}
	
	function read ($id) 
	{
		if ($data = $this->db->query("SELECT data FROM ".$this->session_table." WHERE id='$id' LIMIT 1")->fetch_assoc())
			return $data['data'];
		else
			return '';
	}
	
	function write ($id, $sess_data) 
	{
		return $this->db->query("INSERT INTO ".$this->session_table." (id, user, time, data) VALUES ('$id', '". $_SESSION['id']."', '".time()."', '$sess_data') ON DUPLICATE KEY UPDATE data='$sess_data' , user='".$_SESSION['id']."', time='".time()."'");
	}
	
	function destroy ($id) 
	{
		return $this->db->query("DELETE FROM ".$this->session_table." WHERE id='$id'");
	}
	
	function gc ($maxlifetime) 
	{
	  return $this->db->query("DELETE FROM ".$this->session_table." WHERE (time < ".time()." - $maxlifetime ) OR (time < ".time()." - 86400 AND user='0')");
	}
}

$handler = new DatabaseSessionHandler(db());
session_set_save_handler(array($handler, 'open'),array($handler, 'close'),array($handler, 'read'),array($handler, 'write'),array($handler, 'destroy'),array($handler, 'gc'));

?>