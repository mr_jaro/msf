<?php
/**
 * Modular Simply Framework
 *
 * An open source application development framework for PHP 5.3.9 or newer
 *
 * @author		Jarosław Furmanek
 * @copyright	Copyright (c) 2015, Jarosław Furmanek
 * @license		MIT License
 * @since		Version 0.9.0a
 */


namespace MSF
{
	/*messages*/
	class Messages
	{
		private $msg_er = '';
		private $msg = '';
		
		public function add_msg_er($text)
		{
			if ($this->msg_er != '')
				$this->msg_er .= "<br><br>";
				
			$this->msg_er .= $text;
		}
		
		public function add_msg($text)
		{
			if ($this->msg != '')
				$this->msg .= "<br><br>";
				
			$this->msg .= $text;
		}
		
		public function is_msg_er() 
		{
			if (app()->get_session()->get('msg_er'))
			{
				$this->add_msg_er(app()->get_session()->get('msg_er'));
				app()->get_session()->delete('msg_er');
			}
			
			return $this->msg_er != '' ? TRUE : FALSE;
		}
		
		public function is_msg() {
			if (app()->get_session()->get('msg'))
			{
				$this->add_msg(app()->get_session()->get('msg'));
				app()->get_session()->delete('msg');
			}
			return $this->msg != '' ? TRUE : FALSE; 
		}
		
		public function get_msg_er() {
			return $this->msg_er;
		}
		
		public function get_msg() {
			return $this->msg;
		}
	
	}
	
	/*date time*/
	
	class Date 
	{
		private $timeOffset = 0;
		
		public function __construct($timeOffset)
		{
			$this->timeOffset = $timeOffset;
		}
		
		public function show($time, $hours = true)
		{
			global $timeOffset;
			if ($hours)
				return date('Y/m/d H:i:s', $time + $timeOffset);
			else
				return date('Y/m/d', $time + $timeOffset);
		}
		
		public function show_interval($time, $to = false)
		{
			$time = $to ? $time - time() : time() - $time;
			
			if ($time < 60)
				return $time.' '.locale('seconds');
			if ($time < 3600)
				return date('i', $time).' '.locale('minutes').' '.date('s', $time).' '.locale('seconds');
			if ($time < 86400)
				return date('H', $time).' '.locale('hours').' '.date('i', $time).' '.locale('minutes');
			
			return floor($time / 86400).' '.locale('days').' '.date('H', $time).' '.locale('hours');
		}
		
		function get_offset() {
			return $this->timeOffset;
		}
	}
	
	class Locale
	{
		private $locale = array();
		private $language;
		
		public function __construct($language)
		{
			$this->language = $language;
		}
		
		public function get_language()
		{
			return $this->language;
		}
		
		public function load($path)
		{
			if (file_exists($path))
			{
				require $path;
				
				if (is_array($locale))
				{
					$this->locale = array_merge($this->locale, $locale);
				}
			}
		}
		
		public function get($name)
		{
			if (isset($this->locale[$name]))
				return $this->locale[$name];
			else
				return $name;
		}
	}
}
?>