<?php
/**
 * Modular Simply Framework
 *
 * An open source application development framework for PHP 5.3.9 or newer
 *
 * @author		Jarosław Furmanek
 * @copyright	Copyright (c) 2015, Jarosław Furmanek
 * @license		MIT License
 * @since		Version 0.9.0a
 */
 
require_once 'site/config/config.php';

function app()
{
	global $app;
	return $app;
}

function db($id = 0) 
{
	return app()->get_db($id);
}

function user()
{
	return app()->get_user();
}

function locale($name)
{
	return app()->get_locale()->get($name);
}

function theme()
{
	app()->get_theme();
}

function send_email($to, $title, $text, $html = false, $header = false, $start = '', $stop = '') 
{
	if ($html)
	{
		if (!$header)
		{
			$header = "From: ".app()->config('mail')."\n";
			$header .= "Reply-To: ".app()->config('mail')."\n";
			$header .= "Return-Path: ".app()->config('mail')."\n";
			$header .= "X-Mailer: PHP/" . phpversion();
			$header .= '\nMIME-Version: 1.0' . "n\n"; 
			$header .= 'Content-type: text/html; charset=utf-8' . "\r\n"; 
			$header .= 'Content-Transfer-Encoding: 8bit';
		}
		
		return mail($to, "$title", $start . $text . $stop, $header . "\r\n");
	}
	else
	{
		return mail($to, "$title", $text, 'From: '.app()->config('mail').' <'.app()->config('mail').'>' . "\r\n");
	}
}
?>