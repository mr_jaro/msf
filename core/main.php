<?php
/**
 * Modular Simply Framework
 *
 * An open source application development framework for PHP 5.3.9 or newer
 *
 * @author		Jarosław Furmanek
 * @copyright	Copyright (c) 2015, Jarosław Furmanek
 * @license		MIT License
 * @since		Version 0.9.0a
 */

final class Main 
{
	private static $is_instance = false;
	
	private $address_array = null;
	private $user;
	private $db = null;
	private $session;
	private $date;
	private $messages;
	private $locale;
	private $theme;
	
	public static function get_instance()
    {
        if( self::$is_instance == false )
        {
        	self::$is_instance = true;
            return new main();
        }
		else 
			return false;
    }
	
	public function run($address)
	{
		global $alias;
		
		require 'core/classes.php';
		
		require 'core/database.php';
		$this->db[0] = new MSF\Database();
		
		require 'core/mvc.php';
		require 'core/variable.php';
		
		//session
		require 'core/sessionHandler.php';
		require 'core/session.php';
		$this->session = new MSF\Session();
		
		//user
		require 'site/libraries/user.php';
		$this->user = new MSF\User();
		
		//date time
		$this->date = new MSF\Date(timezone_offset_get( new DateTimeZone( $this->user->timezone ), new DateTime() ));
		
		//messages
		$this->messages = new MSF\Messages();
		
		set_error_handler(array($this, '__my_error_handler'));
		error_reporting(E_ALL | E_STRICT);
		
		define("PATH", ($this->config('path') != '' ? '/'.$this->config('path') : ''));
		define("VERSION", "0.9.0a");

		
		//adresy i ustawienia
		
		define("URL_FULL", "/".$address);
		
		if ($address == '')
		{
			$this->address_array = explode('/',$this->config('index'));
			
			//language
			$lang = $this->config('language');
			$this->locale = new MSF\Locale($lang[0]);
		}
		else
		{
			if ((strpos($address, "$")) || (strpos($address, "\\")) || (strpos($address, ".."))) $this->redirect("/"); //security
			
			$address = preg_replace(array_keys($alias), array_values($alias), $address); //aliases/routing
			
			$this->address_array = explode('/',$address);
			
			//language
			$lang = $this->config('language');
			
			if (in_array($this->address_array[0], $lang))
			{
				$this->locale = new MSF\Locale($this->address_array[0]);
				array_shift($this->address_array);
			}
			else
				$this->locale = new MSF\Locale($lang[0]);

		}

		
		$addr = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://').$_SERVER["SERVER_NAME"]."/".($this->config('path') != '' ? $this->config('path')."/" :'');
		
		define("URL_ROOT", $addr.(isset($this->address_array[0]) ? $this->address_array[0].(isset($this->address_array[1]) && $this->address_array[1] != "index" ? "/".$this->address_array[1] : '') : ''));
		
		//unique ID
		if ((isset($this->address_array[1])) && ($this->address_array[1] != '') && !is_numeric($this->address_array[1]))
			define("UNIQUE_ID",$this->address_array[0].'_'.$this->address_array[1]);
		else
			define("UNIQUE_ID",$this->address_array[0]);
		
		$page = null;
		$arg_arr = null;
		$theme_body = 'body';
		$title = '';
		
		if (count($this->address_array) > 0)
		{
			
			if (!isset($this->address_array[1]))
				$this->address_array[1] = 'index';
			
			//zgodność ze starszymi wersjami c_ oznacza kontroler wzorca mvc jeżeli jest bez c znaczy że nie ma zastosowanego mvc
			if (file_exists("site/modules/".$this->address_array[0]."/c_".$this->address_array[0].".php"))
			{
				include_once "site/modules/".$this->address_array[0]."/c_".$this->address_array[0].".php";
				$class = 'C_'.$this->address_array[0];
				$page = new $class();
				$setup_page = $page->_config();
				
				//lista argumentów
				if (method_exists($page,$this->address_array[1]))
				{
					$arg_arr = $this->create_arr_args(2);
				}
				else
				{
					$arg_arr = $this->create_arr_args(1);
					$this->address_array[1] = 'index';
				}
		
				//theme
				require 'core/theme.php';
				
				if (isset($setup_page[$this->address_array[1]]['theme']))
					$this->theme = new MSF\Theme($setup_page[$this->address_array[1]]['theme']);
				else
					$this->theme = new MSF\Theme($this->config('theme'));
				
				require_once 'site/config/auto_load.php';
				
				//dostep
				$this->user->is_blocked_ip();
				
				if (isset($setup_page[$this->address_array[1]]['role']))
				{
					$this->user->user_access($setup_page[$this->address_array[1]]['role'], true);
				}
				
				//locale
				$this->locale->load("site/locale/".$this->locale->get_language()."/global.php");
				if (isset($setup_page[$this->address_array[1]]['locale']))
				{
					if ($setup_page[$this->address_array[1]]['locale'] === TRUE)
						$this->locale->load("site/modules/".$this->address_array[0]."/locale/".$this->locale->get_language().".php");
					else
						$this->locale->load("site/locale/".$this->locale->get_language()."/".$setup_page[$this->address_array[1]]['locale'].".php");
				}
				
				//szkielet strony
				if (isset($setup_page[$this->address_array[1]]['body']))
					$this->theme->set_file_body($setup_page[$this->address_array[1]]['body']);
				
				//tytuł
				if (isset($setup_page[$this->address_array[1]]['title']))
					$this->theme->set_title($setup_page[$this->address_array[1]]['title']);

				call_user_func_array(array($page, $this->address_array[1]), $arg_arr);

				$content = ob_get_contents();
		    	ob_end_clean();
				$this->theme->generate_page($content);
			}
			else 
				$this->app->redirect('/404');
		}
	}
	
	private function create_arr_args($start)
	{
		$arr = array();
		$count = count($this->address_array);
		for ($i = $start, $j = 0; $i < $count; ++$i, ++$j)
			$arr[$j] = $this->address_array[$i];
		return $arr;
	}
	
	public function __my_error_handler($type, $info, $file, $line) //errors
	{
		$error = "Error ".$type." ".$info." ".$file." ".$line;
		$this->add_log($error);
		
		if ($this->user->role > 1)
			$this->messages->add_msg_er($error);
	}
	
	public function get_part_addres($nr) 
	{
		if (isset($this->address_array[$nr]))
			return $this->address_array[$nr];
		else
			return NULL;
	}
	
	public function get_user()
	{
		return $this->user;
	}
	
	public function get_db($id = 0)
	{
		return $this->db[$id];
	}
	
	public function get_session()
	{
		return $this->session;
	}
	
	public function get_date()
	{
		return $this->date;
	}
	
	public function get_messages()
	{
		return $this->messages;
	}
	
	public function get_locale()
	{
		return $this->locale;
	}

	public function get_theme()
	{
		return $this->theme;
	}
		
	public function config($name)
	{
		global $config;
		if (isset($config[$name]))
		{
			$temp = $config[$name];
			return $temp;
		}
		else
			return null;
	}
	
	public function add_log($info, $id = 0, $user = -1)
	{
		$variable = new MSF\Variable();
		
		if ($this->config('logs'))
		{
			if ($user == -1)
				$user = $this->user->id;
			
			$this->db[0]->query("INSERT INTO ".$this->db[0]->get_prefix()."logs (user, info, time, ip) VALUES ('".$user."', '".$variable->filter_text($info)."', '".time()."', '".$variable->filter_text($_SERVER['REMOTE_ADDR'])."')", $id);
		}
	}
	
	public function redirect($address = '/')
	{
		if ($this->get_messages()->get_msg_er() != '') $this->get_session()->add('msg_er', $this->get_messages()->get_msg_er());
		if ($this->get_messages()->get_msg() != '') $this->get_session()->add('msg', $this->get_messages()->get_msg());
		
		if (FALSE !== strpos($address, '://'))
			header("Location: ".$address);
		else 
			header("Location: ".PATH.$address);
	
		die();
	}
	
	public function load_library($name) {
		include "site/libraries/$name.php";
	}
}
?>