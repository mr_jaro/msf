
<?php

ini_set("session.name", "sess");
ini_set("session.hash_function", "1");
ini_set("session.gc_maxlifetime", 10800);
ini_set("session.cookie_lifetime", 10800);
ini_set('session.use_cookies', 1);
ini_set('session.use_only_cookies', 1);
ini_set('session.use_trans_sid', 0);
ini_set('session.cookie_httponly',1);


$database = array (
	array (
	'host' => '',
	'user' => '',
	'pwd' => '',
	'dbname' => '',
	'prefix' => '',
	'charset' =>''
	),
);

$config = array (
	'index' => 'index',
	'theme' => 'example',
	'page_name' => 'MSF',
	'page_domain' => 'http://localhost',
	'path' => '',
	'logs' => TRUE,
	'session_table' => 'db_sessions',

	'language' => array ('pl', 'en'),
);

$alias = array (
	'#404#si' => 'error/er_404',
	'#403#si' => 'error/er_403',
);

$config['mail_html_header'] = "";
$config['mail_html_footer'] = "";

?>