<?php
	$locale['page_previous'] = 'Poprzednia';
	$locale['page_next'] = 'Następna';
	$locale['page_first'] = 'Pierwsza';
	$locale['page_last'] = 'Ostatnia';
	$locale['select'] = 'Wybierz';
	$locale['edit'] = 'Edytuj';
	$locale['delete'] = 'Usuń';
	$locale['new'] = 'Nowy';
	$locale['add'] = 'Dodaj';
	$locale['save'] = 'Zapisz';
	$locale['send'] = 'Wyślij';
	$locale['action'] = 'Akcja';
	$locale['action_error'] = 'Akcja zakończona niepowodzeniem';
	$locale['added'] = 'Dodano';
	$locale['updated'] = 'Zaktualizowano';
	$locale['removed'] = 'Usunięto';
	$locale['activated'] = 'Aktywowano';
	$locale['deactivated'] = 'Deaktywowano';
	$locale['active'] = 'Aktywuj';
	$locale['deactive'] = 'Deaktywuj';
	$locale['del_question'] = 'Czy na pewno chcesz to usunąć?';
	$locale['name'] = 'Nazwa';
	$locale['description'] = 'Opis';
	$locale['show'] = 'Pokaż';
	$locale['empty'] = 'Brak';
	$locale['creation'] = 'Tworzenie';
	$locale['editing'] = 'Edytowanie';
	$locale['yes'] = 'Tak';
	$locale['no'] = 'Nie';
	$locale['seconds'] = 'sekund';
	$locale['minutes'] = 'minut';
	$locale['hours'] = 'godzin';
	$locale['days'] = 'dni';
?>