<?php
	class C_Error extends _Controller {
		
		function er_404()
		{
			$this->load_view('404');
		}
		
		function er_403()
		{
			$this->load_view('403');
		}
	}

?>