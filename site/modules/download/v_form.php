<form method="post" action="<?= URL_ROOT ?>">
	<table>
		<tr>
			<td>Nazwa</td>
			<td><input type='text' name='name' value='<?= $name ?>' required /></td>
		</tr>
		<tr>
			<td>Link</td>
			<td><input type='text' name='link' value='<?= $link ?>' /></td>
		</tr>
		<tr>
			<td>Nie pokazuj na stronie</td>
			<td><input type='checkbox' name='noview' <?= ($noview == 0 ? '' : "checked" ) ?> /></td>
		</tr>
		<tr>
			<td></td>
			<td class="right" ><input type="submit" value="<?= (isset($id) ? 'Zapisz' : 'Dodaj')  ?>"/></td>
		</tr>
	</table>
	<?= (isset($id) ? "<input type='hidden' name='id' value='$id' />" : '') ?>
</form>