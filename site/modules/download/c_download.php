<?php
	class C_Download extends _Controller {
		
		protected $model;
		
		public function _config() 
		{
			$this->model = $this->load_model();
			
			$page['index'] = array (
				'role' => '2',
			);
			
			return $page;
		}
		
		public function index($action = '', $id = 0)
		{
			$vars = new MSF\Variables();
			
			
			if($values = $vars->post()->filter_text(array('name', 'link')))
			{
				$values['noview'] = $vars->post()->is_set('noview') ? 1 : 0;
				
				if ($vars->post()->is_int('id'))
				{
					//update
					
					if ($this->model->set($values, $_POST['id']))
					{
						$this->app->get_messages()->add_msg("Zaktualizowano");
						$this->app->add_log("Zaktualizowano pozycję w pobieralni - ".$values['name']);
					}
					else
						$this->app->get_messages()->add_msg_er(locale('action_error'));
				}
				else
				{
					//insert
					
					if ($this->model->add($values))
					{
						$this->app->get_messages()->add_msg("Dodano");
						$this->app->add_log("Dodano nową pozycję do pobierani - ".$values['name']);
					}
					else
						$this->app->get_messages()->add_msg_er(locale('action_error'));
				}
				
				$this->redirect();
			}			
			
			if ($action != '')
			{
				if ($action == 'new')
				{
					//create
					
					$this->app->get_theme()->set_title("Download - Nowy");
				
					$values = array(
						'name' => '',
						'link' => '',
						'noview' => 0
					);
					
					$this->load_view('form', $values);
					
				}
				elseif ($vars->variable()->is_int($id))
				{
					if ($action == "edit")
					{
						//edit
						
						$this->app->get_theme()->set_title("Download - Edytowanie");
						
						if ($result = $this->model->get($id))
							$this->load_view('form', $result);
						else
							$this->redirect();
					}
					elseif ($action == "del")
					{
						//delete
						
						if ($this->model->del($id))
						{
							$this->app->get_messages()->add_msg("Usunięto");
							$this->app->add_log("Usunięto pozycję z pobieralni");
							$this->redirect();
						}
						else
						{
							$this->app->get_messages()->add_msg_er(locale('action_error'));
							$this->redirect();
						}
					}
				}
			}
			else 
			{
				$this->app->get_theme()->set_title('Download');
				$this->load_view('index', array('result' => $this->model->get_all() ));				
			}
		}
	}

?>