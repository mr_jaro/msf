<?php
	class m_download extends _Model 
	{
	
		function get_all()
		{
			return $this->db->select('download')->fetch_all(MYSQLI_ASSOC);
		}
		
		function get($id)
		{
			return $this->db->select('download','*', "id=$id")->fetch_assoc();
		}
		
		function del($id)
		{
			return $this->db->delete('download',"id=$id");
		}
		
		function add($values_arr) 
		{
			return $this->db->insert('download', $values_arr);
		}
		
		function set($values_arr, $id) 
		{
			return $this->db->update('download', $values_arr,"id=$id");
		}
	}

?>