<?php
	class C_Index extends _Controller {
		function _config() 
		{
			$page['index'] = array (
				'title' => 'Home'
			);
			return $page;
		}
		
		function index()
		{
			$this->load_view('index');
		}
	}

?>