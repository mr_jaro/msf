<?php
namespace MSF
{
	class User {
		public $id;
		public $name;
		public $role;
		public $timezone;

		public function __construct()
		{			
			$this->id = 0;
			$this->name = "anonymous";
			$this->role = 0;
			$this->timezone = 'Europe/London';
		}

		public function user_access($access, $redirect = false)
		{
			if ($redirect)
			{
				if ($access > $this->role)
				{
					app()->redirect('/403');
				}
			}
			else 
				return $access > $this->role;
		}
		
		
		public function is_blocked_ip()
		{
			
		}
	}
}

?>