<?php
/**
 * Modular Simply Framework
 *
 * An open source application development framework for PHP 5.3.9 or newer
 *
 * @author		Jarosław Furmanek
 * @copyright	Copyright (c) 2015, Jarosław Furmanek
 * @license		MIT License
 * @since		Version 0.9.0a
 */
 
function generate_pagination($addr, $all_items, $on_page, $current_page, $addr_end = '', $icons = false)
{
	$content = '';
	$count = ceil($all_items / $on_page);
	
	if ($icons)
	{
		$page_first = '<img src="'.THEME_URL.'/images/icons/page_first.png" alt="" />';
		$page_previous = '<img src="'.THEME_URL.'/images/icons/page_previous.png" alt="" />';
		$page_next = '<img src="'.THEME_URL.'/images/icons/page_next.png" alt="" />';
		$page_last = '<img src="'.THEME_URL.'/images/icons/page_last.png" alt="" />';
	}
	else
	{
		$page_first = locale('page_first');
		$page_previous = locale('page_previous')." &lt;&lt;";
		$page_next = "&gt;&gt; ".locale('page_next');
		$page_last = locale('page_last');
	}

	$addr = PATH.$addr;
	if ($count > 1)
	{
		$content .= "<div class='pager'>\n";
		
		if ($count < 10)
		{
			if ($current_page > 1) $content .= "<a href='$addr".($current_page - 1)."$addr_end'>$page_previous</a>\n";
			for ($i = 1; $i <= $count; ++$i)
			{
				if ($current_page == $i)
					$content .= "<span class='current_page'>$i</span>\n";
				else
					$content .= "<a href='$addr$i$addr_end'>$i</a>\n";
			}
			if ($count > $current_page) $content .= "<a href='$addr".($current_page + 1)."$addr_end'>$page_next</a>\n";
		}
		else 
		{
			if ($current_page > 1) 
			{
				$content .= "<a href='$addr".($current_page - 1)."$addr_end'>$page_previous</a>&nbsp;&nbsp;\n";
				$content .= "<a href='$addr".(1)."$addr_end'>$page_first</a>\n";
			}
			
			if ($current_page > 5) $content .= " ... ";
			
			$start = ($current_page > 4 ? $current_page - 4 : 1);
			$stop = ($start + 8 <= $count ? $start + 8 : $count);
			for ($i = $start; $i <= $stop; ++$i)
			{
				if ($current_page == $i)
					$content .= "<span class='current_page'>$i</span>\n";
				else
					$content .= "<a href='$addr$i$addr_end'>$i</a>\n";
			}			
			
			if ($current_page < $count - 5) $content .= " ... ";
			
			if ($count > $current_page) 
			{
				$content .= "<a href='$addr".($count)."$addr_end'>$page_last</a>&nbsp;&nbsp;\n";
				$content .= "<a href='$addr".($current_page + 1)."$addr_end'>$page_next</a>\n";
			}
		}
		
		$content .= "</div>\n";
	}
	
	return $content;
}
?>