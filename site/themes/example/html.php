<!DOCTYPE html>
<html dir="ltr">
<head>
	<meta charset='utf-8' >
	<title><?= $title ?></title>
	<meta name="description" content="<?= $description ?>">
	<?= $style ?>
	<?= $script ?>
	
</head>
<body>

	<?= $body ?>
	
</body>
</html>