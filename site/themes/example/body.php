<div id="page_title"><?= $page_title ?></div>

<div class="body" id="<?= UNIQUE_ID ?>">
	<?php if (app()->get_messages()->is_msg_er()) : ?>
		<div id="msg-er">
			<?= app()->get_messages()->get_msg_er() ?>
		</div>
	<?php endif ?>
	
	<?php if (app()->get_messages()->is_msg()) : ?>
		<div id="msg">
			<?= app()->get_messages()->get_msg() ?>
		</div>
	<?php endif ?>
	
	<?= $content ?>
</div>