<?php
/**
 * Modular Simply Framework
 *
 * An open source application development framework for PHP 5.3.9 or newer
 *
 * @author		Jarosław Furmanek
 * @copyright	Copyright (c) 2015, Jarosław Furmanek
 * @license		MIT License
 * @since		Version 0.9.0a
 */
 
	include "core/main.php";
	date_default_timezone_set('UTC');
	require_once 'core/include.php';
	ob_clean();
	ob_start();
	
	$app = Main::get_instance();
	$app->run(isset($_GET['q']) ? $_GET['q'] : '');
?>